# eval_pysur
Evaluates fits constructed by pySurrogate.

**NOTE: This repo has been moved to [Github](https://github.com/sxs-collaboration/eval_pysur), use that instead!** This repository is now read-only.